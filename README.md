# Type-along: Enough Sleep lab

This repo is an attempt to have a type-along session. You can start by using git clone, like so:

```git clone https://tomaccosheep@bitbucket.org/tomaccosheep/enough\_sleep.git enough\_sleep\_copy1```

If you fall behind and we can't figure out why your program doesn't work, then make a copy of the most updated version with this:

```git clone https://tomaccosheep@bitbucket.org/tomaccosheep/enough\_sleep.git enough\_sleep\_copy2``` 
